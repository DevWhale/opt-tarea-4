import sys
import gurobipy as gb
from gurobipy import GRB
import pandas as pd
from scipy.spatial import distance_matrix
import matplotlib
from matplotlib.pylab import plt
from numpy import ndarray

matplotlib.use("QtAgg")


def create_copy_depot(instance_info: dict):
    for i in range(instance_info["t"]):
        depot = instance_info["nodes"][i + instance_info["n"]]
        instance_info["nodes"].append(depot)
    return instance_info


def parser(filename: str, n: int, two_flow: bool):
    instance_info = {}
    file = open(filename, "r")
    file_data = file.readlines()
    file.close()

    data = file_data[0].split()
    # getting the first line info
    instance_info["type"] = int(data[0])
    if n == 25:
        instance_info["m"] = 1
    elif n == 40:
        instance_info["m"] = 2
    instance_info["n"] = int(data[2])
    instance_info["t"] = int(data[3])

    # getting the depot info
    depots = []
    for line in range(instance_info["t"]):
        data = file_data[line + 1].split()
        depots.append([int(i) for i in data])
    instance_info["depots"] = depots

    # getting node info
    nodes_infor = []
    # get n costumers
    for line in range(n):
        data = file_data[line + 1 + instance_info["t"]].strip().split()
        nodes_infor.append(
            [
                int(data[0]),
                float(data[1]),
                float(data[2]),
                int(data[3]),
                int(data[4]),
                int(data[5]),
                int(data[6]),
                data[7:],
            ]
        )
    # get t depots
    for line in range(instance_info["t"]):
        data = (
            file_data[line + 1 + instance_info["t"] + instance_info["n"]]
            .strip()
            .split()
        )
        nodes_infor.append(
            [
                int(data[0]),
                float(data[1]),
                float(data[2]),
                int(data[3]),
                int(data[4]),
                int(data[5]),
                int(data[6]),
            ]
        )
    instance_info["n"] = n
    instance_info["nodes"] = nodes_infor
    if two_flow:
        return create_copy_depot(instance_info)
    return instance_info


def create_problem_dataframe(instance_info: dict):
    df = pd.DataFrame(
        {
            "id": [i[0] for i in instance_info["nodes"]],
            "x": [i[1] for i in instance_info["nodes"]],
            "y": [i[2] for i in instance_info["nodes"]],
        }
    )
    return df


def create_distance_matrix(df: pd.DataFrame):
    matrix = pd.DataFrame(
        distance_matrix(df[["x", "y"]].values, df[["x", "y"]].values, p=2),
        index=df.index,
        columns=df.index,
    ).values
    return matrix


def plot_instance_state(df: pd.DataFrame, margin: int):

    plt.figure(figsize=(12, 12))
    for _, row in df.iterrows():
        if row["id"] > 48:
            plt.scatter(row["x"], row["y"], c="r")
            plt.text(row["x"] + 1, row["y"] + 1, "depot")
        else:
            plt.scatter(row["x"], row["y"], c="black")
            id = row["id"]
            plt.text(row["x"] + 1, row["y"] + 1, f"({id})")

    plt.xlim([min(df["x"].values) - margin, max(df["x"].values) + margin])
    plt.ylim([min(df["y"].values) - margin, max(df["y"].values) + margin])
    plt.title("points: id")
    plt.show()


# Three-index formulation for the MDVRP
def three_index(instance_info: dict, d: ndarray):
    total_nodes = [i for i in range(instance_info["t"] + instance_info["n"])]
    total_cars = [i for i in range(instance_info["m"] * instance_info["t"])]
    total_costumers = total_nodes[: instance_info["n"]]
    depots = [(i + instance_info["n"], i) for i in range(instance_info["t"])]
    depots_car = [
        total_cars[i[1] * instance_info["m"] : (i[1] + 1) * instance_info["m"]]
        for i in depots
    ]
    car_cap = [
        instance_info["depots"][0][1] for _ in depots for _ in range(instance_info["m"])
    ]

    # create the model
    m = gb.Model("MDVRP")
    # adding variables
    x = m.addVars(
        [
            (i, j, k)
            for i in total_nodes
            for j in total_nodes
            for k in total_cars
            if i != j
        ],
        vtype=GRB.BINARY,
        name="x",
    )

    u = m.addVars(
        [i for i in total_costumers],
        vtype=GRB.INTEGER,
        lb=0,
        ub=instance_info["n"],
        name="u",
    )

    m.setObjective(
        gb.quicksum(
            x[i, j, k] * d[i][j]
            for i in total_nodes
            for j in total_nodes
            for k in total_cars
            if i != j
        ),
        GRB.MINIMIZE,
    )
    # costumer online visited once
    m.addConstrs(
        gb.quicksum(x[i, j, k] for i in total_nodes for k in total_cars if i != j) == 1
        for j in total_costumers
    )
    m.addConstrs(
        gb.quicksum(x[i, j, k] for j in total_nodes for k in total_cars if i != j) == 1
        for i in total_costumers
    )
    # route continuity
    m.addConstrs(
        gb.quicksum(x[i, h, k] for i in total_nodes if i != h)
        - gb.quicksum(x[h, j, k] for j in total_nodes if j != h)
        == 0
        for h in total_nodes
        for k in total_cars
    )
    # check vehicle capacity
    m.addConstrs(
        gb.quicksum(
            instance_info["nodes"][i][4] * x[i, j, k]
            for i in total_costumers
            for j in total_nodes
            if i != j
        )
        <= car_cap[k]
        for k in total_cars
    )

    # check route duration
    m.addConstrs(
        gb.quicksum(
            instance_info["nodes"][i][3] * x[i, j, k]
            for i in total_nodes
            for j in total_nodes
            if i != j
        )
        + gb.quicksum(
            d[i][j] * x[i, j, k] for i in total_nodes for j in total_nodes if i != j
        )
        <= instance_info["depots"][0][0]
        for k in total_cars
    )
    m.addConstrs(
        gb.quicksum(x[i[0], j, k] for j in total_costumers if i[0] != j) <= 1
        for i in depots
        for k in depots_car[i[1]]
    )

    m.addConstrs(
        gb.quicksum(x[i, j[0], k] for i in total_costumers if i != j[0]) <= 1
        for j in depots
        for k in depots_car[j[1]]
    )

    m.addConstrs(
        gb.quicksum(x[i, j[0], k] for i in total_costumers if i != j[0]) == 0
        for j in depots
        for k in total_cars
        if k not in depots_car[j[1]]
    )

    m.addConstrs(
        gb.quicksum(x[i[0], j, k] for j in total_costumers if i[0] != j) == 0
        for i in depots
        for k in total_cars
        if k not in depots_car[i[1]]
    )

    # # MTZ formulation for MDVRP
    m.addConstrs(
        u[i] - u[j] + instance_info["n"] * x[i, j, k] <= instance_info["n"] - 1
        for i in total_costumers
        for j in total_costumers
        for k in total_cars
        if i != j
    )
    m.Params.Threads = 1
    m.Params.TimeLimit = 10_800
    m.optimize()

    return (m.objVal, len(m.getConstrs()), len(m.getVars()), m.Runtime)


def two_commodity(instance_info: dict, d: ndarray):
    total_nodes = [
        i for i in range(len(instance_info["nodes"]))
    ]  # indices de los nodos
    total_cars = [
        i for i in range(instance_info["m"] * instance_info["t"])
    ]  # indice de autos
    total_costumers = total_nodes[: instance_info["n"]]  # indice customer
    depots = [
        (i + instance_info["n"], i) for i in range(instance_info["t"])
    ]  # indice depots
    copy_depots = [
        (i[0] + instance_info["t"], i[1]) for i in depots
    ]  # indice de copy depts
    depots_car = [
        total_nodes[i[1] * instance_info["m"] : (i[1] + 1) * instance_info["m"]]
        for i in depots
    ]  # autos por depot
    car_cap = [
        instance_info["depots"][0][1]
        for _ in total_cars
        for _ in range(instance_info["m"])
    ]  # capcidad en cada auto

    m = gb.Model("MDVRP")
    x = m.addVars(
        [
            (i, j, k)
            for i in total_nodes
            for j in total_nodes
            for k in total_cars
            if i != j
        ],
        vtype=GRB.BINARY,
        name="x",
    )
    y = m.addVars(
        [
            (i, j, k)
            for i in total_nodes
            for j in total_nodes
            for k in total_cars
            if i != j
        ],
        vtype=GRB.INTEGER,
        lb=0,
        # ub=instance_info["m"],  # preguntar si aqui esta la capacidad
        name="y",
    )

    z = m.addVars(
        [(i, k) for i in total_nodes for k in total_cars],
        vtype=GRB.BINARY,
        name="z",
    )

    m.setObjective(
        0.5
        * gb.quicksum(
            x[i, j, k] * d[i][j]
            for i in total_nodes
            for j in total_nodes
            for k in total_cars
            if i != j
        ),
        GRB.MINIMIZE,
    )

    # constraint 2
    m.addConstrs(
        gb.quicksum(y[j, i, k] - y[i, j, k] for j in total_nodes if i != j)
        == 2 * instance_info["nodes"][i][4] * z[i, k]
        for i in total_costumers
        for k in total_cars
    )

    # Constraint 3

    m.addConstr(
        gb.quicksum(
            y[i[0], j, k]
            for i in depots
            for j in total_costumers
            for k in total_cars
            if i != j
        )
        == gb.quicksum(instance_info["nodes"][j][4] for j in total_costumers)
    )
    # Constraint 4
    m.addConstr(
        gb.quicksum(
            y[i[0], j, k]
            for i in depots
            for j in total_costumers
            for k in total_cars
            if i != j
        )
        <= gb.quicksum(car_cap[k] for k in total_cars)
        - gb.quicksum(instance_info["nodes"][j][4] for j in total_costumers)
    )
    # Constraint 5
    m.addConstrs(
        gb.quicksum(y[i[0], j, k] for j in total_costumers if i[0] != j) <= car_cap[k]
        for i in copy_depots
        for k in depots_car[i[1]]
    )
    # Constraint 6
    m.addConstrs(
        gb.quicksum(x[i, j, k] for i in total_nodes if i != j) == 2 * z[j, k]
        for j in total_costumers
        for k in total_cars
    )
    # Constraint 7
    m.addConstrs(
        y[i, j, k] + y[j, i, k] == car_cap[k] * x[i, j, k]
        for i in total_nodes
        for j in total_nodes
        for k in total_cars
        if i != j
    )
    # Constraint 8
    m.addConstrs(gb.quicksum(z[i, k] for k in total_cars) == 1 for i in total_costumers)

    # Constraint 9
    m.addConstrs(
        y[i, j, k] <= 999999 * z[i, k]
        for i in total_costumers
        for j in total_nodes
        for k in total_cars
        if i != j
    )

    # constraint 10
    m.addConstrs(
        gb.quicksum(
            instance_info["nodes"][i][3] * x[i, j, k]
            for i in total_costumers
            for j in total_nodes
            if i != j
        )
        + gb.quicksum(
            d[i][j] * x[i, j, k] for i in total_nodes for j in total_nodes if i != j
        )
        <= 2 * instance_info["depots"][0][0]
        for k in total_cars
    )

    # Constraint 11
    m.addConstrs(
        gb.quicksum(x[i[0], j, k] for j in total_costumers if i[0] != j) <= 1
        for i in depots
        for k in depots_car[i[1]]
    )

    # Constraint 12
    m.addConstrs(
        gb.quicksum(x[i, j[0], k] for i in total_costumers if i != j[0]) == 0
        for j in copy_depots
        for k in total_cars
        if k not in depots_car[j[1]]
    )
    # Constraint 13
    m.addConstrs(
        gb.quicksum(x[i[0], j, k] for j in total_costumers if i[0] != j) == 0
        for i in depots
        for k in total_cars
        if k not in depots_car[i[1]]
    )

    m.Params.Threads = 1
    m.Params.TimeLimit = 10_800
    m.optimize()

    return (m.objVal, len(m.getConstrs()), len(m.getVars()), m.Runtime)


def plot_results(tir: tuple, tcr: tuple):

    left = [2, 4]
    time = [tir[3], tcr[3]]
    constr = [tir[1], tcr[1]]
    vars = [tir[2], tcr[2]]
    label = ["Three-Index Formulation", "Two-Comodity Flow"]
    color = ["blue", "red"]
    # ploting time vs instance
    plt.bar(
        left,
        time,
        tick_label=label,
        width=0.8,
        color=color,
    )
    plt.xlabel("Modelos")
    plt.ylabel("Tiempo de execucion (segundos)")
    plt.title("Comparacion de tiempos de ejecucion")
    plt.show()
    # ploting constr vs modelos
    plt.bar(
        left,
        constr,
        tick_label=label,
        width=0.8,
        color=color,
    )
    plt.xlabel("Modelos")
    plt.ylabel("Numero de Restricciones")
    plt.title("Comparacion de Numero de Restricciones")
    plt.show()

    # ploting variables vs modelos
    plt.bar(
        left,
        vars,
        tick_label=label,
        width=0.8,
        color=color,
    )
    plt.xlabel("Modelos")
    plt.ylabel("Numero de Variables")
    plt.title("Comparacion de Numero de variables")
    plt.show()


def save_on_file(tir: tuple, tcr: tuple, file: str, n: int):
    results = open("results.txt", "a")
    instance = f"{file.split('/')[1].split('.')[0]}_{n}"
    results.write(instance + "\t Three-index\t Two-Commodity\n")
    results.write(f"objValue\t {tir[0]}\t {tcr[0]}\n")
    results.write(f"tiempo(s)\t {tir[3]}\t {tcr[3]}\n")
    results.write(f"Restricciones\t {tir[1]}\t {tcr[1]}\n")
    results.write(f"Variables\t {tir[2]}\t {tcr[2]}\n")
    results.close()


if __name__ == "__main__":
    filename = sys.argv[1]
    n = int(sys.argv[2])

    instance_info_TC = parser(filename, n, True)
    instance_info_TI = parser(filename, n, False)

    df_TC = create_problem_dataframe(instance_info_TC)
    d_TC = create_distance_matrix(df_TC)

    df_TI = create_problem_dataframe(instance_info_TI)
    d_TI = create_distance_matrix(df_TI)
    tir = three_index(instance_info_TI, d_TI)
    tcr = two_commodity(instance_info_TC, d_TC)
    # plot_results(tir, tcr)
    save_on_file(tir, tcr, filename, n)
